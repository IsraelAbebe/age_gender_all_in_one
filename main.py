from keras import Input
from keras.models import load_model
from keras.models import Model
import numpy as np
import cv2
import os
import sys

def facecrop(image):
    facedata = "haarcascade_frontalface_alt.xml"
    cascade = cv2.CascadeClassifier(facedata)

    img = cv2.imread(image)

    minisize = (img.shape[1], img.shape[0])
    miniframe = cv2.resize(img, minisize)

    faces = cascade.detectMultiScale(miniframe)

    for f in faces:
        x, y, w, h = [v for v in f]
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 255, 255))

        sub_face = img[y:y + h, x:x + w]
        fname, ext = os.path.splitext(image)
        cv2.imwrite(fname + "_cropped_" + ext, sub_face)

    return fname + "_cropped_" + ext


def predict(image):
    image = facecrop(image)
    age_class = [["0-10", "10-20","20-30","30-40", "40-55", "55-65", "65-90"]]
    gen_class = [["Female", "Male"]]

    model_age = load_model('models/age.h5')
    # model_age.get_layer(name= 'model').name='predictions_1'

    model_gender = load_model('models/gender2.h5')
    # model_gender.get_layer(name='model_1').name='predictions_2'

    model_age.name = 'model_age'
    model_gender.name = 'model_gender'

    x = Input(shape=[100, 100, 3])
    y_age = model_age(x)
    y_gen = model_gender(x)

    model = Model(inputs=x, outputs=[y_age, y_gen])

    data = cv2.imread(image)
    data = cv2.resize(data, (100, 100))
    p_age, p_gender = model.predict(np.expand_dims(data, 0))

    age_ = np.where(p_age == 1)
    gen_ = np.where(p_gender == 1)
    print(age_class[int(age_[0])][int(age_[1])] + " " + gen_class[int(gen_[0])][int(gen_[1])])


if __name__ == "__main__":
    predict(sys.argv[1])
